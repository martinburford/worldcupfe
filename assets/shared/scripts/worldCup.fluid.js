/**
* World Cup v1.0
* @module worldCup
*/

var worldCup = window.worldCup || {};

/**
* worldCup root class
* @class worldCup
* @namespace worldCup.fluid
*/

worldCup.fluid = (function(){
	var configuration = {
		// ...
	};
		
	/**
	* Initialize all fluid grid functionality
	* @method init
	*/
	function init(){
		$.logEvent('[worldCup.fluid.init]');
		
		worldCup.configuration.deviceType = 'fluid';
		
console.log('[worldCup.fluid.init]');
	}
	
	/**
	* Unload all associated event handlers relating to mobile
	* @method unload
	*/
	function unload(){
		$.logEvent('[worldCup.fixed.unload]');
console.log('[worldCup.fluid.unload]');
	}
		
	return {
		configuration: configuration,
		init: init,
		unload: unload
	}
}());