/**
* World Cup v1.0
* @module worldCup
*/

var worldCup = window.worldCup || {};

/**
* worldCup root class
* @class worldCup
* @namespace worldCup.fixed
*/

worldCup.fixed = (function(){
	var configuration = {
		// ...
	};
		
	/**
	* Initialize all fixed pixel functionality
	* @method init
	*/
	function init(){
		$.logEvent('[worldCup.fixed.init]');
		
		worldCup.configuration.deviceType = 'fixed';
		
console.log('[worldCup.fixed.init]');
	}
	
	/**
	* Unload all associated event handlers relating to mobile
	* @method unload
	*/
	function unload(){
		$.logEvent('[worldCup.fixed.unload]');
console.log('[worldCup.fixed.unload]');
	}
		
	return {
		configuration: configuration,
		init: init,
		unload: unload
	}
}());