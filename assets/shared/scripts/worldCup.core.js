/**
* World Cup v1.0
* @module worldCup
*/

var worldCup = window.worldCup || {};
worldCup.configuration = {
	debugLogging: false,
	siteInitialized: false
};

/**
* worldCup root class
* @class worldCup
* @namespace core
*/
worldCup.core = (function(){
	/** 
	* Initialize worldCup
	* @method init
	* @param {STRING} deviceType Either 'fluid' || 'static'
	*/	
	function init(obj){	
		$.logEvent('[worldCup.core.init]: ' + $.logJSONObj(obj));
				
		// Update the global site configuration, taking values as specified within the HTML source-code
		worldCup.configuration.deviceType = obj.deviceType;
		
		// Ensure that the dimensions/mode layer are updated on resize
		modeDimensionsRefresh();
		
		worldCup.configuration.siteInitialized = true;
	};

	/**
	* Initialize functionality for the active device
	* Unload all functionality for the non-active device
	* @method deviceInit
	* @param {String} deviceType 'fluid' || 'fixed'
	*/
	function deviceInit(obj){
		$.logEvent('[worldCup.core.deviceInit]: ' + $.logJSONObj(obj));
		
		// Upon every device switch, scroll the page to the top, and remove the 'pinnedToTop' classname
		$(window).scrollTo(0);
				
		switch(obj.deviceType){
			case 'fluid':
				worldCup.fixed.unload();
				worldCup.fluid.init();
				
				break;
			case 'fixed':
				worldCup.fluid.unload();
				worldCup.fixed.init();
				
				break;
		}
	}
	
	/**
	* Ensure that the dimensions/mode layer are updated on resize
	* @method modeDimensionsRefresh
	*/
	function modeDimensionsRefresh(){	
		// Which mode does the current resolution fall into
		var currentWidth = $(window).width();
		var whichMode = null;
		
		if($.numberInRange({number:currentWidth, lower:0, upper: 767})){
			whichMode = 'fluid';
		}
		else{
			whichMode = 'fixed';
		}
		
		$('#mode-dimensions #mode SPAN').html(whichMode);
		$('#mode-dimensions #dimensions SPAN').html($(window).width() + 'x' + $(window).height());
	}
	
	/**
	* All functionality to load once the correct CSS and JavaScript has been attached to the DOM (via adaptJS)
	* @method postCSSLoadInit
	*/
	function postCSSLoadInit(){
		$.logEvent('[worldCup.core.postCSSLoadInit]');
	}

	return {
		deviceInit: deviceInit,
		init: init,
		modeDimensionsRefresh: modeDimensionsRefresh,
		postCSSLoadInit: postCSSLoadInit
	}
}());

// Window events
$(window).on({
	load: function(){		
		// All functionality to load once the correct CSS and JanvaScript has been attached to the DOM (via adaptJS)
		worldCup.core.postCSSLoadInit();
	},
	resize: function(){
		// Ensure that the dimensions/mode layer are updated on resize
		worldCup.core.modeDimensionsRefresh();
	}
});

// jQuery extensions
$.extend({
	/**
	* Logging, based on whether it has been configured to log or not
	* @method logEvent
	* @param {STRING} Event The event to log
	*/
	logEvent: function(event){
		if(worldCup.configuration.debugLogging){
			$('#logger .inner')
				.prepend(
					$('<p />')
						.html(($('#logger .inner P').size()+1) + '. ' + event)
				)
		}
	},
	
	/**
	* Loop through an object
	* @method logJSONObj
	* @param {OBJECT} obj A variable JSON object to output to the console
	*/
	logJSONObj: function(obj){
		var debugJSON = '';
		var i;
		
		for(i in obj){
			if(obj.hasOwnProperty(i)){
				debugJSON += i + '=' + obj[i] + ', ';	
			}
		}
		return debugJSON.length > 0 ? debugJSON.substr(0,debugJSON.length-2) : '[empty parameter object]';
	},
	
	/**
	* Is a number between 2 numbers?
	* @method numberInRange
	* @param {INTEGER} number The number to check whether it is within the lower and upper bounds
	* @param {INTEGER} lower The lower number to check against
	* @param {INTEGER} upper The upper number to check against
	*/	
	numberInRange: function(obj){		
		return obj.number >= obj.lower && obj.number <= obj.upper;
	}
});

$.fn.extend({
	IF: function(expr){
		return this.pushStack((this._ELSE = !($.isFunction(expr) ? expr.apply(this) : expr))? [] : this, 'IF', expr);
	},
	
	ELSE: function(expr){
		var $set = this.end();
		return $set.pushStack(((!$set._ELSE) || ($set._ELSE = ((typeof(expr) !== 'undefined') && (!($.isFunction(expr) ? expr.apply($set) : expr)))))? [] : $set, 'ELSE', expr);
	},

	ENDIF: function(){
		return this.end();
	},
	
	hasAttr: function(name){  
		return this.attr(name) !== undefined;
	},
	
	hasParent: function(filterCriteria){
		return this.parents(filterCriteria).length;
	}
});