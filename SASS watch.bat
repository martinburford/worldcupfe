@REM OUTPUT STYLES: --style nested || --style expanded || --style compact || --style compressed

@REM SINGLE DIRECTORY COMPILATIONS
@REM sass --watch css:css/css-compiled-nested --style nested
@REM sass --watch css:css/css-compiled-expanded --style nested
@REM sass --watch css:css/css-compiled-compact --style nested
@REM sass --watch css:css/css-compiled-compressed --style nested

@REM Move to local directory for compilation
cd C:\inetpub\wwwroot\freelancing\trinity-mirror\match-centre-integrated\

sass --watch assets/shared/css:assets/shared/css/compiled assets/shared/css/utilities:assets/shared/css/utilities/compiled assets/adaptive/css:assets/adaptive/css/compiled _components/assets/css:_components/assets/css/compiled --style expanded